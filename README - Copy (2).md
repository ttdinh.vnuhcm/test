QUỐC HỘI 

Luật số: ………/2023/QH15
(Dự thảo 2)	CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM
Ðộc lập - Tự do - Hạnh phúc



LUẬT
sửa đổi, bổ sung một số điều 
của luật CÔNG AN NHÂN DÂN SỐ  37/2018/QH14

Căn cứ Hiến pháp nước Cộng hòa xã hội chủ nghĩa Việt Nam;
Quốc hội ban hành Luật sửa đổi, bổ sung một số điều của Luật Công an nhân dân số 37/2018/QH14.

Điều 1. Sửa đổi, bổ sung một số điều của Luật Công an nhân dân 
1. Sửa đổi, bổ sung khoản 4 Điều 22 như sau:
“4. Sĩ quan được xét thăng cấp bậc hàm từ Đại tá lên Thiếu tướng phải còn ít nhất đủ 03 năm công tác; trường hợp không đủ 03 năm công tác do Chủ tịch nước quyết định”.
2. Sửa đổi, bổ sung khoản 1 Điều 23 như sau:
“1. Sĩ quan, hạ sĩ quan, chiến sĩ Công an nhân dân lập thành tích đặc biệt xuất sắc trong bảo vệ an ninh quốc gia, bảo đảm trật tự, an toàn xã hội, đấu tranh phòng, chống tội phạm và vi phạm pháp luật, xây dựng Công an nhân dân, nghiên cứu khoa học, công tác, học tập mà cấp bậc hàm hiện tại thấp hơn cấp bậc hàm cao nhất đối với chức vụ, chức danh sĩ quan, hạ sĩ quan, chiến sĩ đang đảm nhiệm thì được xét thăng cấp bậc hàm trước thời hạn.
Chính phủ quy định cụ thể tiêu chí, tiêu chuẩn lập thành tích đặc biệt xuất sắc trong bảo vệ an ninh quốc gia, bảo đảm trật tự, an toàn xã hội, đấu tranh phòng, chống tội phạm và vi phạm pháp luật, xây dựng Công an nhân dân, nghiên cứu khoa học, công tác, học tập để xét thăng cấp bậc hàm cấp Tướng trước thời hạn.”.
3. Sửa đổi, bổ sung Điều 25 như sau:
a) Sửa đổi, bổ sung các điểm b, d và e khoản 1 như sau:
“b) Thượng tướng không quá 07, gồm: Thứ trưởng Bộ Công an, số lượng không quá 06; Sĩ quan Công an nhân dân biệt phái được phê chuẩn chức vụ Chủ nhiệm Ủy ban Quốc phòng và An ninh của Quốc hội.”.
“d) Thiếu tướng không quá 162 bao gồm:
Cục trưởng của các đơn vị trực thuộc Bộ Công an và chức vụ, chức danh tương đương, trừ quy định tại điểm c khoản 1 Điều này;
Giám đốc Công an tỉnh, thành phố trực thuộc trung ương ở địa phương được phân loại đơn vị hành chính cấp tỉnh loại I và là địa bàn trọng điểm, phức tạp về an ninh, trật tự, diện tích rộng, dân số đông. Số lượng không quá 11;
Phó Chủ nhiệm Ủy ban Kiểm tra Đảng ủy Công an Trung ương. Số lượng không quá 03;
Phó Cục trưởng, Phó Tư lệnh và tương đương của các đơn vị trực thuộc Bộ Công an quy định tại điểm c khoản 1 Điều này. Số lượng: 17 đơn vị mỗi đơn vị không quá 04, các đơn vị còn lại mỗi đơn vị không quá 03;
Phó Cục trưởng và tương đương của các đơn vị trực thuộc Bộ Công an, trừ quy định tại điểm c khoản 1 Điều này. Số lượng không quá 02;
Phó Giám đốc Công an thành phố Hà Nội, Phó Giám đốc Công an Thành phố Hồ Chí Minh. Số lượng mỗi đơn vị không quá 03;
Sĩ quan Công an nhân dân biệt phái được phê chuẩn chức vụ Ủy viên Thường trực Ủy ban Quốc phòng và An ninh của Quốc hội hoặc được bổ nhiệm chức vụ Tổng cục trưởng hoặc tương đương;”. 
“e) Thượng tá: Trưởng phòng và tương đương; Trưởng Công an huyện, quận, thị xã thuộc tỉnh, thành phố trực thuộc trung ương; Trưởng Công an thành phố thuộc tỉnh.”.
b) Sửa đổi, bổ sung khoản 2 như sau:
“2. Ủy ban Thường vụ Quốc hội quy định cụ thể vị trí có cấp bậc hàm cao nhất là Trung tướng, Thiếu tướng chưa được quy định cụ thể trong Luật này; quy định cấp bậc hàm cấp Tướng đối với đơn vị được thành lập mới.”.
c) Sửa đổi, bổ sung khoản 4 như sau:
“4. Trưởng phòng và tương đương ở các đơn vị thuộc cơ quan Bộ có chức năng, nhiệm vụ trực tiếp chiến đấu, tham mưu, nghiên cứu, hướng dẫn chuyên môn, nghiệp vụ toàn lực lượng; Trưởng phòng tham mưu, nghiệp vụ, Trưởng Công an quận thuộc Công an thành phố Hà Nội, Công an Thành phố Hồ Chí Minh, Trung đoàn trưởng, Trưởng Công an thành phố thuộc thành phố trực thuộc trung ương có cấp bậc hàm cao hơn một bậc quy định tại điểm e khoản 1 Điều này.”.
4. Sửa đổi, bổ sung Điều 30 như sau:
a) Sửa đổi tiêu đề như sau:
“Điều 30. Hạn tuổi phục vụ của công nhân công an, hạ sĩ quan, sĩ quan Công an nhân dân”
b) Sửa đổi, bổ sung khoản 1 như sau:
“1. Hạn tuổi phục vụ cao nhất của công nhân công an, hạ sĩ quan, sĩ quan Công an nhân dân quy định như sau:
a) Công nhân công an: Nam 62, nữ 60.
b) Hạ sĩ quan: 47;
c) Cấp úy: 55;
d) Thiếu tá, trung tá: Nam 57, nữ 55;
đ) Thượng tá: Nam 60, nữ 58;
e) Đại tá: Nam 62, nữ 60;
g) Cấp tướng: Nam 62; nữ 60;
Lộ trình tăng hạn tuổi phục cao nhất: Mỗi năm tăng 03 tháng đối với nam quy định tại các điểm a, e, g khoản này và 04 tháng đối với nữ quy định tại các điểm a, đ, e khoản này. Các trường hợp còn lại thực hiện tăng hạn tuổi phục vụ cao nhất khi Luật này có hiệu lực thi hành.
Thời điểm tăng hạn tuổi phục vụ cao nhất tính từ ngày 01/01/2021 nhưng không áp dụng đối với trường hợp đã nghỉ công tác trước ngày Luật này có hiệu lực thi hành. 
Chính phủ quy định cụ thể lộ trình tăng hạn tuổi phục vụ cao nhất của sĩ quan, hạ sĩ quan, công nhân công an.”.
c) Sửa đổi, bổ sung khoản 3 như sau:
 “3. Trường hợp đơn vị công an có nhu cầu, sĩ quan quy định tại điểm c, điểm d và điểm đ khoản 1 Điều này nếu có đủ phẩm chất, giỏi về chuyên môn, nghiệp vụ, có sức khỏe tốt và tự nguyện thì có thể được kéo dài tuổi phục vụ theo quy định của Bộ trưởng Bộ Công an, nhưng không quá 62 tuổi đối với nam và 60 đối với nữ. 
Trường hợp đặc biệt sĩ quan quy định tại khoản 1 Điều này có thể được kéo dài hơn 62 tuổi đối với nam, 60 đối với nữ theo quyết định của cấp có thẩm quyền”.
d) Sửa đổi, bổ sung khoản 4 như sau:
“4. Sĩ quan Công an nhân dân là giáo sư, phó giáo sư, tiến sĩ, chuyên gia cao cấp có thể được kéo dài hạn tuổi phục vụ hơn 62 đối với nam và hơn 60 đối với nữ theo quy định của Chính phủ”.
5. Sửa đổi, bổ sung khoản 2 Điều 42 như sau:
“2. Chính phủ quy định chế độ, chính sách của công nhân công an”.
Điều 2. Hiệu lực thi hành
Luật này có hiệu lực thi hành kể từ ngày……. tháng……. năm……..
Luật này được Quốc hội nước Cộng hòa xã hội chủ nghĩa Việt Nam khóa XV, kỳ họp thứ … thông qua ngày ….. tháng…… năm ………
CHỦ TỊCH QUỐC HỘI

       Vương Đình Huệ